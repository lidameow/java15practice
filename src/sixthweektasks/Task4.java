package sixthweektasks;

import java.util.Scanner;

public class Task4 {
    /*
    На вход подается число N — ширина и высота матрицы.
    Затем передается сама матрица,
    состоящая из натуральных чисел.

    Необходимо изменить её так,
    чтобы она стала симметричной
    относительно главной диагонали
    (отобразить элементы,
    расположенные над главной диагональю вниз).
    Главную диагональ заполнить нулями.

    Главной диагональю называется диагональ,
    проходящая из верхнего левого угла в правый нижний.

    Вход
    3
    1 2 3
    4 5 6
    7 8 9

(0, 1) - элемент 2 i j
(1, 0) - элемент 4 j i

(0, 2) - элемент 3
(2, 0) - элемент 7

arr[j][i] = arr[i][j]

    Выход
    0 2 3
    2 0 6
    3 6 0
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    arr[i][j] = 0;
                } else {
                    arr[j][i] = arr[i][j];
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
