package sixthweektasks;

import java.util.Scanner;

public class Task3 {
    /*
        На вход подается число N -
    ширина и высота матрицы.
    Затем передается сама матрица,
    состоящая из натуральных чисел.
    После этого передается натуральное число P.

    Необходимо найти в матрице число
    P и занулить строку и столбец,
    в котором это число находится (кроме числа P).
    Применить эту операцию ко всем найденным числам P.

    Вход
    3
    1 2 3
    1 5 3
    1 2 3
    5

    Выход
    1 0 3
    0 5 0
    1 0 3
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }

        int p = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] == p) {
                    fillWithZero(arr, n, i, j, p);
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void fillWithZero(int[][] a, int n, int i, int j, int p){
        // Зануляем строку
        /*
            1 2 3
            1 5 3
            1 2 3
         */
        for (int k = 0; k < n; k++) {
            if (a[i][k] != p) {
                a[i][k] = 0;
            }
        }
        /*
            1 2 3
            0 5 0
            1 2 3
         */

        // Зануляем столбец
        for (int k = 0; k < n; k++) {
            if (a[k][j] != p) {
                a[k][j] = 0;
            }
        }
        /*
            1 0 3
            0 5 0
            1 0 3
         */
    }
}
