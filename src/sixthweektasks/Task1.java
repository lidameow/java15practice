package sixthweektasks;

import java.util.Arrays;
import java.util.Scanner;

public class Task1 {
    /*
    На вход передается
    N — высота двумерного массива и
    M — его ширина.
    Затем передается сам массив.

    Необходимо сохранить в одномерном массиве
    суммы чисел каждого столбца и вывести их на экран.

    Вход
    2 2
    10 20
    5  7

    Выход
    15 27

    Вход
    3 1
    30
    42
    15

    Выход
    87
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] arr = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }

        int[] res = new int[m];
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                res[j] += arr[i][j];
            }
        }
        System.out.println(Arrays.toString(res));
    }
}
