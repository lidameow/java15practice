package sixthweektasks;

import java.util.Random;
import java.util.Scanner;

public class Task7 {
    /*
    На вход передается M — количество мест
    в одном ряду кинотеатр и N — количество рядов.
    Необходимо заполнить кинотеатр размера N на M случайным
    заполнением (0 — свободное место, 1 — занятое).

    Петя со своей подругой хочет купить два билета в кино рядом.
    Необходимо вывести на экран предзаполненные
    места кинотеатра и после этого проверить,
    найдутся ли подходящие места для Пети и его подруги.

    Подходящими местами являются два свободных места рядом
    в одном ряду.
     */

    // 0 1 0 1 0 0

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] arr = new int[n][m];
        Random r = new Random();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j] = r.nextInt(2);
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }

        boolean answer = false;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m - 1; j++) {
                if (arr[i][j] == 0 && arr[i][j + 1] == 0) {
                    answer = true;
                }
            }
        }
        System.out.println(answer);
    }
}
