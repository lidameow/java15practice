package sixthweektasks;

import java.util.Scanner;

public class Task5 {
    /*
       На вход подается число N —
    количество участников соревнования и M —
    количество голосующих в жюри.
    Далее в каждой из N строк (соответствующие i-му участнику)
    передается по M чисел — оценка каждого жюри за i-го участника.


    Необходимо вывести номер участника-победителя
    (тот, кто набрал больше всех голосов).
    Если таковых несколько, то вывести максимальный номер.
    Важно: нумерация участников начинается с 1.

    Вход
    3 5
    5 1 4 4 5
    2 2 3 2 2
    5 5 5 3 5

    Выход
    3

    Вход
    2 2
    10 8
    9 9

    Выход
    2
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] arr = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }

        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                res[i] += arr[i][j];
            }
        }

        int max = 0;
        int idx = 0;
        // 1 5 2 6
        for (int i = 0; i < n; i++) {
            if (res[i] >= max) {
                max = res[i];
                idx = i;
            }
        }
        System.out.println(idx + 1);
    }
}
