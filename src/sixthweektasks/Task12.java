package sixthweektasks;

import java.util.Scanner;

public class Task12 {
    /*
    На вход подается два целых числа a и b
    Написать функцию через рекурсию для
    вычисления суммы заданных положительных
    целых чисел a b без прямого использования
    оператора + между ними.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println(sum(a,b));
    }

    public static int sum(int a, int b) {
        if (b == 0) {
            return a;
        }
        return sum(a + 1, b - 1);
    }
}
