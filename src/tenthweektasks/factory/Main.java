package tenthweektasks.factory;

public class Main {
    public static void main(String[] args) {
        IceCreamFactory iceCreamFactory = new IceCreamFactory();

        IceCream cherryIceCream = iceCreamFactory.getIceCream(IceCreamType.CHERRY);
        IceCream chocolateIceCream = iceCreamFactory.getIceCream(IceCreamType.CHOCOLATE);
        IceCream vanillaIceCream = iceCreamFactory.getIceCream(IceCreamType.VANILLA);

        cherryIceCream.printIngredients();
        chocolateIceCream.printIngredients();
        vanillaIceCream.printIngredients();

        System.out.println("------------------------------------");
        IceCream cherry = iceCreamFactory.getIceCream(CherryIceCream.class);
        IceCream chocolate = iceCreamFactory.getIceCream(ChocolateIceCream.class);
        cherry.printIngredients();
        chocolate.printIngredients();

        System.out.println("------------------------------------");
        // Так делать не надо!!!
        IceCream c = iceCreamFactory.getIceCream(new ChocolateIceCream());

    }
}
