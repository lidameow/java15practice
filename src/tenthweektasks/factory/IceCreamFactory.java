package tenthweektasks.factory;

/*
Реализация паттерна фабрика
 */
public class IceCreamFactory {
    // Можно сделать конструктор приватным и метод статическим
    //private IceCreamFactory(){};
    public IceCream getIceCream(IceCreamType type) {
        IceCream iceCream = null;
        switch (type) {
            case CHERRY:
                iceCream = new CherryIceCream();
                break;
            case CHOCOLATE:
                iceCream = new ChocolateIceCream();
                break;
            case VANILLA:
                iceCream = new VanillaIceCream();
                break;
        }
        return iceCream;
    }

    public IceCream getIceCream(Class obj) {
        IceCream iceCream = null;
        if (obj.equals(ChocolateIceCream.class)) {
            iceCream = new ChocolateIceCream();

        }

        if (obj.equals(CherryIceCream.class)) {
            iceCream = new CherryIceCream();
        }

        if (obj.equals(VanillaIceCream.class)) {
            iceCream = new VanillaIceCream();
        }
        return iceCream;
    }

    // Как делать не надо!!!!!
    public IceCream getIceCream(Object obj) {
        IceCream iceCream = null;
        if (obj instanceof ChocolateIceCream) {
            iceCream = new ChocolateIceCream();
        }

        if (obj instanceof CherryIceCream) {
            iceCream = new CherryIceCream();
        }

        if (obj instanceof VanillaIceCream) {
            iceCream = new VanillaIceCream();
        }
        return iceCream;
    }



}
