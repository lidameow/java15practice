package tenthweektasks.factory;

public enum IceCreamType {
    CHERRY,
    CHOCOLATE,
    VANILLA;
}
