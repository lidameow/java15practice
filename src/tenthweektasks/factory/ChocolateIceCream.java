package tenthweektasks.factory;

public class ChocolateIceCream implements IceCream{
    @Override
    public void printIngredients() {
        System.out.println("Chocolate, cream, ice, love");
    }
}
