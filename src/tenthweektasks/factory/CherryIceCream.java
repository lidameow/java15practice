package tenthweektasks.factory;

public class CherryIceCream implements IceCream{
    @Override
    public void printIngredients() {
        System.out.println("Cherry, cream, ice, love");
    }
}
