package tenthweektasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        ArrayList<Car> cars = new ArrayList<>();
        cars.add(new Car("VOLVO"));
        cars.add(new Car("BMW"));
        cars.add(new Car("AUDI"));

        for (Car car : cars) {
            System.out.println(car.getModel());
        }

        // Другая форма записи через лямбду
        //cars.forEach(car -> {
        //    System.out.println(car.getModel());
        //});

        cars.remove(cars.size() - 1);
        System.out.println("После удаления");
        for (Car car : cars) {
            System.out.println(car.getModel());
        }

        System.out.println(cars.contains(new Car("BMW")));
        Car currentCar = cars.get(0);
        System.out.println(cars.contains(currentCar));



        Collections.sort(cars, new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.getModel().compareTo(o2.getModel());
            }
        });

        Collections.sort(cars, ((o1, o2) -> o1.getModel().compareTo(o2.getModel())));
        for (Car car : cars) {
            System.out.println(car.getModel());
        }








    }
}
