package tenthweektasks.logger;

public class ConsoleLogger implements Logger{

    @Override
    public void log(String message) {
        System.out.println(message);
    }

    public void hello() {
        System.out.println("hello");
    }
}
