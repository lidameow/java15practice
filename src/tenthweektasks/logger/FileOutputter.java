package tenthweektasks.logger;

public abstract class FileOutputter {
    private final String fileName = "default_file_name";

    public String getFileName(){
        return fileName;
    }

    public abstract String getFileExtention();
}
