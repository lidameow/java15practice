package tenthweektasks.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

// TXT
public class FileLogger extends FileOutputter implements Logger {
    private String filename;

    public FileLogger() {
        this.filename = getFileName() + getFileExtention();
    }

    public FileLogger(String filename) {
        this.filename = filename;
    }

    @Override
    public void log(String message) {
        try(Writer writer = new FileWriter(filename, true)) {
            writer.write(message + "\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getFileExtention() {
        return ".txt";
    }
}
