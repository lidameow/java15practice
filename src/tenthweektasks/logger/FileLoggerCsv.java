package tenthweektasks.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileLoggerCsv extends FileOutputter implements Logger{
    private String filename;

    public FileLoggerCsv() {
        this.filename = getFileName() + getFileExtention();
    }

    public FileLoggerCsv(String filename) {
        this.filename = filename;
    }

    @Override
    public void log(String message) {
        try(Writer writer = new FileWriter(filename, true)) {
            writer.write(message + "\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getFileExtention() {
        return ".csv";
    }
}
