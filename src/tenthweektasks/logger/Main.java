package tenthweektasks.logger;

import eighthweek.Bulb;

public class Main {
    public static void main(String[] args) {
        //ConsoleLogger cl = new ConsoleLogger();
        Logger cl = new ConsoleLogger();
        ((ConsoleLogger)cl).hello();

        Logger fl = new FileLogger();
        Bulb bulb = new Bulb(fl);

        bulb.turnOn();
        bulb.turnOff();
        bulb.getCurrentState();


    }
}
