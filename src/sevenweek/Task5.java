package sevenweek;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Task5 {
    /*
    Конкурс красоты собак
     */
    public static void main(String[] args) {
        int n = 4;
        String[] imena = new String[]{"Иван", "Николай", "Анна", "Дарья"};
        String[] klichki = new String[]{"Жучка", "Кнопка", "Цезарь", "Добряш"};

        int[][] ozenki = new int[][]{{7, 6, 7}, {8, 8, 7}, {4, 5, 6}, {9,9,9}};

        double[] srednee = new double[n];
        for (int i = 0; i < n; i++) {
            double sum = 0;
            for (int j = 0; j < 3; j++) {
                sum += ozenki[i][j];
            }
            sum /= 3;
            srednee[i] = sum;
        }

        double[] mst = new double[n];
        for (int i = 0; i < n; i++) {
            mst[i] = ozenki[i][0];
        }
        Arrays.sort(mst);
        System.out.println(Arrays.toString(mst));

        System.out.println(Arrays.toString(srednee));

        /*
        1) Выбираем первый максимум по индексу
          - Объявляем максимальный элемент (max = srednee[0], maxIndex = 0)
          - Идем в цикле и если встречаем элемент, который больше чем max то
            приравниваем max к элементу который встретили и обновляем maxIndex
        2) Присвоим этому элементу -1
        [-1, -1, 5.0, -1]


        // Сортируем по второй колонке
        0   7
        1   3
        2   4
        3   2

        0   7
        2   4
        1   3
        3   2
         */
    }
}
