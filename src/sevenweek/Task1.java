package sevenweek;

import java.util.Scanner;

/*
Дано натуральное число n.
Выведите все числа от 1 до n.
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(recPrint(n));
    }

    public static String recPrint(int n){
        // Базовый случай
        if (n == 1) {
            return "1";
        }
        return recPrint(n - 1) + " " + n;
    }
}

// recPrint(n) = recPrint(n - 1) + " " + n
// recPrint(n - 1) = recPrint(n - 2) + " " + (n - 1)
// ..
// ..
// recPrint(1) = 1

// recPrint(3) = 1 + " " + 2 + " " + 3
// recPrint(2) = 1 + " " + 2
// recPrint(1) = 1

