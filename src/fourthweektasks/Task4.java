package fourthweektasks;

import java.util.Scanner;

public class Task4 {
    /*
    Начальный вклад в банке равен 1000.
    Каждый месяц размер вклада увеличивается
    на P процентов от имеющейся суммы (0 < P < 25).
    Найти через какое количество времени размер
    вклада будет больше 1100 и вывести
    найденное количество месяцев и итоговый размер вклада.

    Входные данные
    15
    Выходные данные
    1
    1150.0

    Входные данные
    3
    Выходные данные
    4
    1125.50881
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int p = scanner.nextInt();

        double start = 1000;
        double limit = 1100;
        int count = 0;

        while (start <= limit) {
            start += start * p / 100.0;
            count++;
        }
        System.out.println("Количество месяцев " + count);
        System.out.println("Итоговая сумма " + start);
    }
}
