package fourthweektasks;

import java.util.Scanner;

public class Task5 {
    /*
    Дано целое число n.
    Вывести сумму всех цифр этого числа.
    Входные данные
    92180
    Выходные данные
    20

    Входные данные
    52775
    Выходные данные
    26
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }
        System.out.println(sum);
    }
}
