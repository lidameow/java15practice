package eighthweek.temptype;

public enum TempType {
    FAHRENHEIT,
    CELSIUS
}
