package eighthweek.temptype;

/*
Реализовать класс “Термометр”.
Необходимо иметь возможность создавать экземпляр
класса с текущей температурой и получать значение
в фаренгейте и в цельсии.
 */
public class Thermometer {
    private double tempCelsius;
    private double tempFahrenheit;

    public double getTempCelsius() {
        return tempCelsius;
    }

    public double getTempFahrenheit() {
        return tempFahrenheit;
    }

    public Thermometer(double currentTemp, String tempType) {
        if (tempType.equals("C")) {
            tempCelsius = currentTemp;
            tempFahrenheit = convertCelsiusToFahrenheit(currentTemp);
        } else if (tempType.equals("F")) {
            tempFahrenheit = currentTemp;
            tempCelsius = convertFahrenheitToCelsius(currentTemp);
        } else {
            //System.out.println("Пожалуйста введите корректные значения");
            throw new IllegalArgumentException("Введен неверный маркер температуры");
        }
    }

    public Thermometer(double currentTemp, TempType tempType) {
        if (tempType.equals(TempType.CELSIUS)) {
            tempCelsius = currentTemp;
            tempFahrenheit = convertCelsiusToFahrenheit(currentTemp);
        } else if (tempType.equals(TempType.FAHRENHEIT)) {
            tempFahrenheit = currentTemp;
            tempCelsius = convertFahrenheitToCelsius(currentTemp);
        }
    }

    private double convertCelsiusToFahrenheit(double tempInCelsius){
        return tempInCelsius * 1.8 + 32;
    }

    private double convertFahrenheitToCelsius(double tempInFahrenheit) {
        return (tempInFahrenheit - 32) / 1.8;
    }
}
