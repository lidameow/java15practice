package eighthweek;

import tenthweektasks.logger.ConsoleLogger;
import tenthweektasks.logger.FileLogger;
import tenthweektasks.logger.Logger;

/*
Реализовать класс “Лампа”. Методы:
включить лампу
выключить лампу
получить текущее состояние
 */
public class Bulb {
    private Logger logger;
    private boolean toggle;

    public Bulb(Logger logger) {
        this.logger = logger;
    }

    public void turnOn() {
        toggle = true;
        logger.log("Bulb is on");
    }

    public void turnOff() {
        toggle = false;
        logger.log("Bulb is off");
    }

    public boolean getCurrentState() {
        //System.out.println("Получаем текущее состояние");
        logger.log("Getting current state");
        return toggle;
    }
}
