package eighthweek;
// https://refactoring.guru/ru - паттерны проектирования простым языком
// Вернет вам состояние вашего экземпляра всегда в единственном числе
public class Singleton {
    // Поле для хранения инстанса
    private static Singleton instance;

    // Обязательно делаем конструктор private чтобы
    // мы не смогли создать еще один экземпляр класса Singleton
    private Singleton(){}

    // Метод для получения экземпляра класса
    public static Singleton getInstance() {
        // При первом вызове экземпляр создается и кладется
        // в поле instance
        if (instance == null) {
            instance = new Singleton();
        }
        // При последующих вызовах instance != null
        // Просто возвращается instance
        return instance;
    }
}
