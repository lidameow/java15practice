package secondWeekTasks;

import java.util.Scanner;

public class Task5 {
    /*
     Даны три целых числа a, b, c.
     Проверить есть ли среди них прямо противоположные.
     ( 5 и -5 прямо противоположные числа)
     Входные данные
    -1 1 0
    Выходные данные
    true
    Входные данные
    -2 1 0
    Выходные данные
    false
    Входные данные
    -2 2 0
    Выходные данные
    true
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println("Входные данные");
        System.out.println(a + " " + b + " " + c);
        System.out.println((a == -b && a != 0) || (a == -c && a != 0) || (c == -b && c != 0));
    }
}
