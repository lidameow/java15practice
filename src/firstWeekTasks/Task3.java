package firstWeekTasks;

import java.util.Scanner;

public class Task3 {
    /*
    Напишите программу, которая получает два
    числа с плавающей точкой х и у и
    выводит евклидово расстояние от точки (х, у) до точки (0, 0)

    Входные данные
    x = 7 y = 5
    Выходные данные
    8.602325267042627

    Входные данные
    x = 2 y = 6
    Выходные данные
    6.324555320336759

    Y
    ^
   5|_____ a(7, 5)
    |    /|
    |   / |
    |  /  |
    | /   |
    |/--------->
          7      X
     */

    public static void main(String[] args) {
        System.out.println("Введите x");
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        System.out.println("Введите y");
        double y = scanner.nextDouble();

        double result = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        System.out.println("Евклидово расстояние: " + result);
    }
}
