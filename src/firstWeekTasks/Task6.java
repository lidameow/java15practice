package firstWeekTasks;

import java.util.Scanner;

public class Task6 {
    /*
     Перевод литров в галлоны.
     С консоли считывается
     число n  –  количество литров,
     которое нужно перевести в галлоны.
     (1 литр = 0,219969 галлонна)

    Входные данные
    1
    Выходные данные
    0,219969
     */
    public static void main(String[] args) {
        final double GALLONS_IN_LITTER = 0.219969;
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println("Число галлонов: " + n * GALLONS_IN_LITTER);
    }
}
