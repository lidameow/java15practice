package firstWeekTasks;

import java.util.Scanner;

public class Task8 {
    /*
    Вычислить доход работника за неделю.
    Прочитать из консоли данные количество
    рабочих часов в неделю, часовая ставка, налог.
    Параметры
    c - количество рабочих часов в неделю
    r - ставка за час
    t - налог

    Входные данные
    c = 32 r = 1674 t = 18
    Выходные данные
    43925.76
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество рабочих часов в неделю");
        int c = scanner.nextInt();
        System.out.println("Введите ставку за час");
        double r = scanner.nextDouble();
        System.out.println("Введите налог");
        int t = scanner.nextInt();

        double salary = c * r - c * r * t / 100;
        System.out.println("Зарплата: " + salary);
    }
}
