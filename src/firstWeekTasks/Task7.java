package firstWeekTasks;

import java.util.Scanner;

public class Task7 {
    /*
    Дано двузначное число.
    Вывести сначала левую цифру (десятки),
    затем правую (единицы)

    Входные данные
    45
    Выходные данные
    Число десятков: 4
    Число единиц: 5
    45 = (10 * 4) + (1 * 5)
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        System.out.println("Число десятков: " + n / 10);
        System.out.println("Число единиц: " + n % 10);
    }
}
