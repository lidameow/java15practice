package firstWeekTasks;

import java.util.Scanner;

public class Task5 {
    /*
    Напишите аналог функции swap,
    которая меняет значения двух параметров местами
    без вспомогательной переменной.
    Достаточно написать именно логику и
    вывести новые значения переменных.

    Входные данные
    a = 8 b = 10
    Выходные данные
    a = 10 b = 8
     */
    public static void main(String[] args) {
        System.out.println("Введите два числа");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println("Число a: " + a + " число b: " + b);
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("Число a: " + a + " число b: " + b);
    }
}
