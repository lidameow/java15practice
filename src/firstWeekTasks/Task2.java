package firstWeekTasks;

import java.util.Scanner;

public class Task2 {
    /*
    Дано m – количество гигабайт трафика,
    используемое пользователем за месяц, с – заплаченная цена за этот трафик.
    Вычислить стоимость одного гигабайта трафика

    m:c = 1:x
    m / c = 1 / x -> mx = c -> x = c * 1 / m

    Входные данные
    m = 3 c = 22
    Выходные данные
    7.333333333333333
     */
    public static void main(String[] args) {
        System.out.println("Введите количество гигабайт трафика");
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        System.out.println("Введите стоимость трафика");
        int c = scanner.nextInt();

        System.out.println("Стоимость одного гигабайта: " + (double)c * 1 / m);
        System.out.println("Стоимость одного гигабайта: " + c * 1.0 / m);
        System.out.println("Стоимость одного гигабайта: " + c * 1D / m);
    }
}
