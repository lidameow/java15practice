package nineweektasks.customcollections;

/*
Простая реализация LinkedList (однонаправленная)
Методы:
    - get(int index)
    - addAtHead(int val)
    - addAtTail(int val)
    - deleteAtIndex(int index)
    - size()
 */
public class SimpleLinkedList {
    private Node head;  // ссылка на самый первый элемент
    private int size;

    // Класс описывающий ноду
    private static class Node {
        int value;  // У ноды есть значение
        Node next;  // У ноды есть ссылка на следующий элемент

        Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

    public SimpleLinkedList(){
        size = 0;
        head = null;
    }

    public int get(int index) {
        if (index < 0 || index >= size) {
            return  -1;
        }
        // Получаем ссылку на голову
        Node node = head;
        // Ищем нужную ноду на месте index
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node.value;
    }

    // Добавление элемента в голову
    public void addAtHead(int val) {
        // Создаем новую ноду, которая указывает на
        // старую голову
        Node newNode = new Node(val, head);
        // Заставляем голову указывать на вновь созданную ноду
        head = newNode;
        size++;
    }

    // Добавление в конец
    public void addToTail(int val) {
        // если размер больше нуля
        if (size > 0) {
            Node node = head;
            // находим конечную ноду
            for (int i = 0; i < size - 1; i++) {
                node = node.next;
            }
            // создаем новую и делаем ее конечной
            node.next = new Node(val, null);
        } else {
            head = new Node(val, null);
        }
        size++;
    }

    // Добавление по индексу
    public void addAtIndex(int val, int index) {
        if (index < 0 || index > size) {
            return;
        }

        if (index == 0) {
            addAtHead(val);
            return;
        }

        if (index == size) {
            addToTail(val);
            return;
        }

        Node node = head;
        for (int i = 0; i < index - 1; i++) {
            node = node.next;
        }

        Node newNode = new Node(val, node.next);
        node.next = newNode;
        size++;
    }

    public void deleteAtIndex(int index) {
        if (index < 0 || index > size) {
            return;
        }

        if (index == 0) {
            head = head.next;
            size--;
            return;
        }

        Node node = head;
        for (int i = 0; i < index - 1; i++) {
            node = node.next;
        }

        if (index == size - 1) {
            node.next = null;
        } else {
            node.next = node.next.next;
        }

        size--;
    }

    public int size() {
        return size;
    }
}
