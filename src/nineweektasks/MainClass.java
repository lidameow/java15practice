package nineweektasks;

import nineweektasks.customcollections.SimpleArrayList;
import nineweektasks.customcollections.SimpleLinkedList;
import nineweektasks.enums.WeekDays;
import nineweektasks.file.ReadFile;

import java.io.IOException;

public class MainClass {
    public static void main(String[] args) throws IOException {

//        String str = "среда";
//        System.out.println("Порядковый номер дня недели: " + WeekDays.ofName(str).dayNumber);
//        int sr = 3;
//        System.out.println("Названия дня недели: " + WeekDays.ofNumbers(sr).name);
//
//        int illegalDayNumber = -200;
//        try {
//            System.out.println(WeekDays.ofNumbers(illegalDayNumber).name);
//        } catch (IllegalArgumentException e) {
//            System.out.println("Нам пришел неверный аргумент, не падаем");
//        }

        //String filePath = "/Users/l.vygonskaya/IdeaProjects/JavaPracticeTasks/src/nineweektasks/file/input.txt";
        //String windowsPath = "IdeaProjects\\sbcourses\\src\\Week9Practice\\file\\input.txt";
        //ReadFile.readAndWriteDataFromFile(filePath);


//        SimpleArrayList simpleArrayList = new SimpleArrayList(2);
//        simpleArrayList.add(1);
//        simpleArrayList.add(2);
//        simpleArrayList.add(3);
//        simpleArrayList.add(4);
//
//        for (int i = 0; i < simpleArrayList.size(); i++) {
//            System.out.println(simpleArrayList.get(i));
//        }


        SimpleLinkedList simpleLinkedList = new SimpleLinkedList();
        simpleLinkedList.addAtHead(1);
        simpleLinkedList.addAtHead(2);

        System.out.println("Размер " + simpleLinkedList.size());
        System.out.println("Нулевой элемент " + simpleLinkedList.get(0));
        System.out.println("Первый элемент " + simpleLinkedList.get(1));


    }
}
