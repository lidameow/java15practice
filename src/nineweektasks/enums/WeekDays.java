package nineweektasks.enums;

public enum WeekDays {
    MONDAY(1, "понедельник"),
    TUESDAY(2, "вторник"),
    WEDNESDAY(3, "среда"),
    THURSDAY(4, "четверг"),
    FRIDAY(5, "пятница"),
    SATURDAY(6, "суббота"),
    SUNDAY(7, "воскресенье"),
    NOT_A_DAY(-1, "такого дня нет"); // Можно сделать так но не желательно

    private static final WeekDays[] ALL = values();
    public final int dayNumber;

    public final String name;

    WeekDays(int dayNumber, String name) {
        this.dayNumber = dayNumber;
        this.name = name;
    }

    public static WeekDays ofNumbers(int dayNumber) {
        for (WeekDays day: ALL) {
            if (day.dayNumber == dayNumber) {
                return day;
            }
        }
        // NullPointerException так лучше не делать
        // return null;
        throw new IllegalArgumentException("Неизвестное значение номера дня " + dayNumber);
    }

    public static WeekDays ofName(String name) {
        for (WeekDays day: ALL) {
            if (day.name.equals(name)) {
                return day;
            }
        }
        throw new IllegalArgumentException("Неизвестное значение названия дня " + name);
    }
}
