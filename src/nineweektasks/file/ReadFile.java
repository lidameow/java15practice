package nineweektasks.file;

import nineweektasks.enums.WeekDays;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class ReadFile {
    private static final String PKG_DIRECTORY =  "/Users/l.vygonskaya/IdeaProjects/JavaPracticeTasks/src/nineweektasks/file";
    private static final String FILENAME = "output.txt";

    private ReadFile(){};

    public static void readAndWriteDataFromFile(String filePath) throws IOException{
        Scanner scanner = new Scanner(new File(filePath));
        String[] days = new String[10];
        int i = 0;
        while (scanner.hasNextLine()) {
            days[i++] = scanner.nextLine();
        }

        Writer wr = new FileWriter(new File(PKG_DIRECTORY + "/" + FILENAME));
        for (int j = 0; j < i; j++) {
            String res = "Порядковый номер дня недели " + days[j] + " = "
                    + WeekDays.ofName(days[j]).dayNumber + "\n";
            wr.write(res);
        }
        wr.close();
        scanner.close();


        /* try with resources можно почитать
        try(Writer w = new FileWriter("")) {
            // код по работе с файл врайтерой
        }
         */
    }
}
