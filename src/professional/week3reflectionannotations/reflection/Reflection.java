package professional.week3reflectionannotations.reflection;

/*
Определение
Рефлексия (англ. reflection) – знание кода о самом себе
К рефлексии можно отнести возможность проитерироваться по всем полям класса
или найти и создать объект класса, по имени, заданному через текстовую строку

Class – основной класс для рефлексии в Java
 */
public class Reflection {
    public static void main(String[] args) {
        /*
        Три способа получить Class
         */
        
        //1 способ - псевдополе .class
        Class<String> str1 = String.class;
        
        //2 способ - метод getClass() у Object
        //Этот метод учитывает полиморфизм и возвращает реальный класс, которым является объект
        CharSequence sequence = "My String";
        Class<? extends CharSequence> c2 = sequence.getClass(); //вернется Class<String>
        System.out.println(c2);
        
        //3 способ
        //найти класс по строчному имени
        try {
            Class<?> integerClass = Class.forName("java.lang.Integer");
            System.out.println(integerClass);
        }
        catch (ClassNotFoundException e) {
            System.out.println("Нет такого класса");
        }
    }
    
}
