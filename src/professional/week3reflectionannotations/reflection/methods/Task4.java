package professional.week3reflectionannotations.reflection.methods;

public class Task4 {
    int a;
    String b;
    
    /**
     * Конструктор класса Task. Принимает два параметра:
     * @param a - число
     * @param b - строка
     */
    public Task4(int a, String b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public String toString() {
        return "Task4{" +
               "a=" + a +
               ", b='" + b + '\'' +
               '}';
    }
}
