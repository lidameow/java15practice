package professional.week3reflectionannotations.reflection.superclass;

/*
Получение родителей
 - метод getSuperClass() - возвращает Class родителя текущего класса
 - метод getInterfaces() - возвращает список Class'ов интерфейсов, реализуемых текущим классом
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
interface A {}
interface B {}
class C implements A {}
class D extends C implements B {}
 */
public class ReflectionSuperClass {
    public static void main(String[] args) {
        //вывести для класса D все интерфейсы
//        for (Class<?> cls : D.class.getInterfaces()) {
//            System.out.println(cls.getName());
//        }
        
        // задача 1
        // Получить все интерфейсы класса,
        // включая интерфейсы от классов-родителей.
        // Не включать интерфейсы родительских интерфейсов.
        List<Class<?>> result = getAllInterfaces(D.class);
        for (Class<?> cls : result) {
            System.out.println(cls.getName());
        }
    }
    
    
    // задача 1
    // Получить все интерфейсы класса,
    // включая интерфейсы от классов-родителей.
    // Не включать интерфейсы родительских интерфейсов.
    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
