package professional.week3reflectionannotations.reflection.modifiers;

import com.sun.security.jgss.GSSUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/*
Получение модификаторов класса
 - getModifiers() - все модификаторы, с которыми был объявлен класс
 public native int getModifiers();
 
 Модификаторы пакуются внутри битов инта, распаковать информацию можно методами класса Modifier:
public static boolean isPublic(int mod) { }
public static boolean isPrivate(int mod) { }
public static boolean isProtected(int mod) { }
public static boolean isStatic(int mod) { }
public static boolean isFinal(int mod) { }
public static boolean isInterface(int mod) { }
public static boolean isAbstract(int mod) { }

Работа с полями:
Метод getFields() возвращает все публичные поля класса или интерфейса, включая унаследованные:
public Field[] getFields() throws SecurityException

Метод getDeclaredFields() возвращает вообще все поля класса или интерфейса, но исключая унаследованные:
public Field[] getDeclaredFields() throws SecurityException

* SecurityException бросается в случае запрета доступа к пакету, в котором лежит класс

Есть еще методы getField() и getDeclaredField() позволяют найти поле по его строчному имени.

Основные методы класса Field:
get() и set() позволяют прочитать и записать значение поля:
getName() возвращает имя поля
getType() возвращает объект Class для его типа
getModifiers() тоже есть и работает так же, как для Class
 */
public class ReflectionClassModifiers {
    public static void main(String[] args) throws IllegalAccessException {
//        System.out.println(Modifier.isPublic(ReflectionClassModifiers.class.getModifiers()));
        Task task = new Task();
        task.i = 987;
        printAllFields(Task.class, task);
//        printAllFieldsWithValues(Task.class, task);
    }
    
    // задача 2
    // Вывести все поля класса, их модификаторы и типы.
    public static void printAllFields(Class<?> clazz, Task task) throws IllegalAccessException {
        for (Field field : clazz.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPublic(mods)) {
                System.out.print(" public ");
            }
            if (Modifier.isProtected(mods)) {
                System.out.print(" protected ");
            }
            if (Modifier.isPrivate(mods)) {
                System.out.print(" private ");
                field.setAccessible(true);
            }
            if (Modifier.isStatic(mods)) {
                System.out.print(" static (value = " + field.get(null) + ")");
            }
            if (Modifier.isFinal(mods)) {
                System.out.print(" final ");
            }
            System.out.print(" " + field.getType().getName() + " " + field.getName() + ": " + field.get(task));
            System.out.println();
        }
    }
    
    // Задача 3
    // Продолжение задачи 2
    // Создать инстанс класса Task и вывести значения его полей
    public static void printAllFieldsWithValues(Class<?> clazz, Task task) throws IllegalAccessException {
//        printAllFields(clazz);
        System.out.println("\n");
        for (Field field : clazz.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPrivate(mods)) {
                field.setAccessible(true);
            }
            System.out.println(field.get(task));
        }
    }
    
}
