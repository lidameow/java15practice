package professional.week3reflectionannotations.annotations;

import java.util.HashSet;
import java.util.Set;

/*
Определение
Аннотации - это форма метаданных.
Они предоставляют информацию о программе, при том сами частью программы не являются.

Применение
 - Информация для компилятора. Могут использоваться компилятором для обнаружения ошибок
 и подавления предупреждений.
 - Обработка во время компиляции и развертывания.
 Программа может создавать код, XML-файлы и т.п. на основе аннотаций.
 - Обработка во время выполнения. Некоторые аннотации могут использоваться во время выполнения программы.
 
 Стандартные аннотации:
 @Override
 Она используется для проверки переопределенных методов во время компиляции на такие ошибки, как опечатки в регистре символов
 
 @Deprecated
 Аннотация @Deprecated используется для пометки устаревших методов или типов.
IDE автоматически обрабатывают эту аннотацию и обычно отображают устаревший метод зачеркнутым шрифтом, сообщая разработчику, что больше не следует его
использовать.

@SuppressWarnings
Аннотация @SuppressWarnings используется для подавления предупреждений компилятора.
Например, @SuppressWarnings("unchecked") отключает предупреждения, связанные с "сырыми" типами (Raw Types).
TODO:
1) https://docs.oracle.com/javase/7/docs/api/javax/annotation/processing/Processor.html
 */

/**
 * @value = нащи значения
 *
 */
public class Annotations {
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Deprecated
//    @Override
    public static void main(String[] args) {
        Set set = new HashSet();
        set.add(1);
        System.out.println(set.size());
    }
}
