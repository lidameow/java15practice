package professional.week1.exceptions;

public class MyArithmeticException
      extends Exception {
    //extends ArithmeticException {
    public MyArithmeticException() {
    }
    
    public MyArithmeticException(String message) {
        super(message);
    }
}
