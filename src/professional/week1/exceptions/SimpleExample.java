package professional.week1.exceptions;

import java.util.Scanner;

//stacktrace
//https://javastudy.ru/wp-content/uploads/2016/01/exceptionsInJavaHierarchy.png
public class SimpleExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
//        if (n == 0){
//            System.out.println("Число не должно равняться 0");
//            return;
//        }
        // System.out.println(100 / n);
//        try {
//            toDivide(100, n);
//        }
//        catch (ArithmeticException e) {
//            throw new MyArithmeticException("Деление на 0!");
//            //System.exit(0); - не выполнится finally в таком случае
//            //throw new MyArithmeticException(e.getMessage());
//        }
//        finally {
//            System.out.println("Будет ли вывод на экран?");
//        }
        try {
            toDivide(100, n);
        }
        catch (MyArithmeticException e) {
            e.printStackTrace();
        }
        // System.out.println("Будет ли вывод на экран?");
    }
    
    public static void toDivide(int a, int b) throws MyArithmeticException {
        if (b == 0) {
            throw new MyArithmeticException("");
        } else {
            System.out.println(a / b);
        }
        try {
            System.out.println(a / b);
        }
        catch (ArithmeticException e) {
            //System.out.println(e.getMessage());
            throw new MyArithmeticException("Деление на 0!");
        }
        finally {
            System.out.println("Выход из программы!");
        }
    }
    
    //TODO: https://habr.com/ru/post/455796/ - про ООП
}
