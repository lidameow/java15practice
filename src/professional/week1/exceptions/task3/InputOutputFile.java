package professional.week1.exceptions.task3;

import java.io.*;
import java.util.Scanner;

//Read and Write to file
public class InputOutputFile {
    
    private static final String PKG_DIRECTORY = "/Users/andreigavrilov/Work/Projects/СберКурсыШкола/Java15/src/prof/week1" +
                                                "/exceptions/file";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private static final String INPUT_FILE_NAME = "input.txt";
    
    public static void main(String[] args) throws IOException {
        readAndWriteFile();
    }
    
    public static void readAndWriteFile() throws IOException {
        Scanner scanner = new Scanner(new File(INPUT_FILE_NAME));
        Writer writer = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME);
        /*
        В блоке try-with-resources переменные, используемые в качестве ресурсов, должны быть final или effectively final.

Final переменная - это переменная, которой нельзя присвоить новое значение после инициализации.

Effectively final переменная - это переменная, которой можно присвоить новое значение после инициализации, но это не делается в блоке try.

Это правило связано с тем, что переменные-ресурсы могут быть закрыты автоматически, когда блок try завершается, и для этого необходимо иметь доступ к
переменным-ресурсам за пределами блока try. Если переменная-ресурс не является final или effectively final, то компилятор не может гарантировать, что
значение переменной не изменится до момента её закрытия, что может привести к ошибкам в выполнении программы.
         */
        try (scanner;
             writer) {
            // scanner = null; - так нельзя, нужно обязательно закрыть ресурс, а поэтому переменная должна быть final/effectively final
            while (scanner.hasNext()) {
                writer.write(scanner.nextLine() + "\n");
            }
        }
    }
}
