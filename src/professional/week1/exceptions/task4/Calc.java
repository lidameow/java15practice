package professional.week1.exceptions.task4;


import professional.week1.exceptions.task4.exception.MyDivisionByZeroException;
import professional.week1.exceptions.task4.exception.MyInputException;
import professional.week1.exceptions.task4.exception.MyOverflowException;
import professional.week1.exceptions.task4.exception.MyWrongOperatorException;

import java.util.Scanner;

/*
Дан код простенького калькулятора. Нужно:
a) места, где проблемы или исключения

Проблемы:
1) Деление на 0
2) Недопустимый ввод для калькулятора (строка или что-то еще)
3) Число больше диапазона
4) Недопустимый ввод операции (любой символ, вместо + - / *)

b) Обернуть такие места в try и в catch бросить кастомные исключения
с) Продумать логику/иерархию свои исключений так, чтобы для них всех была еще псевдо-запись в лог (логирование)

 */
public class Calc {
    //[JAVA15][ФИО][ДЗ Исключения]
    private static final Scanner scanner = new Scanner(System.in);
    //API
    private int a;
    private int b;
    private char c;
    
    public void input() {
        try {
            a = scanner.nextInt();
            c = scanner.next().charAt(0);
            b = scanner.nextInt();
        }
        catch (RuntimeException e) {
            throw new MyInputException();
        }
    }
    
    public int calculate() {
        return switch (c) {
            case '+' -> sum();
            case '-' -> sub();
            case '/' -> integerDiv();
            case '*' -> mul();
            default -> throw new MyWrongOperatorException();
        };
    }
    
    private int sum() {
        try {
            //return a + b;
            return Math.addExact(a, b);
        }
        catch (ArithmeticException e) {
            throw new MyOverflowException();
        }
    }
    
    private int sub() {
        try {
            return Math.subtractExact(a, b);
        }
        catch (ArithmeticException e) {
            throw new MyOverflowException();
        }
        //return a - b;
    }
    
    private int integerDiv() {
        try {
            return a / b;
        }
        catch (ArithmeticException e) {
            throw new MyDivisionByZeroException();
        }
    }
    
    private int mul() {
        try {
            return Math.multiplyExact(a, b);
        }
        catch (ArithmeticException e) {
            throw new MyOverflowException();
        }
//        return a * b;
    }
    
}


