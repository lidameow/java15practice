package professional.week1.exceptions.task4.exception;

public class MyOverflowException extends MyBaseException{
    public MyOverflowException(String errorMessage) {
        super(errorMessage);
    }
    
    public MyOverflowException(){
        super("Выход за пределы допустимых значений");
    }
}
