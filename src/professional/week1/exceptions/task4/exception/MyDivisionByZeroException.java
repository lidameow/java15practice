package professional.week1.exceptions.task4.exception;

public class MyDivisionByZeroException
      extends MyBaseException {
    public MyDivisionByZeroException(String errorMessage) {
        super(errorMessage);
    }
    
    public MyDivisionByZeroException() {
        super("Недопустимое деление на 0");
    }
}
