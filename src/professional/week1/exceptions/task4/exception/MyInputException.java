package professional.week1.exceptions.task4.exception;

public class MyInputException
      extends MyBaseException {
    public MyInputException(String errorMessage) {
        super(errorMessage);
    }
    
    public MyInputException(){
        super("Неверный формат ввода");
    }
}
