package professional.week1.exceptions.task4.exception;

public class MyBaseException
      extends RuntimeException {
    
    public MyBaseException(String errorMessage) {
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }
}
