package professional.week1.exceptions.task4.exception;

public class MyWrongOperatorException
      extends MyBaseException {
    public MyWrongOperatorException(String errorMessage) {
        super(errorMessage);
    }
    public MyWrongOperatorException(){
        super("Недопустимый оператор для калькулятора");
    }
}
