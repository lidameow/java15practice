package professional.week1.exceptions;

public class MultipleExceptions {
    public static void main(String[] args) {
        try {
            simpleRuntimeException();
            someMethodThrowArrayOutOfBoundException();
            toDivideThrowMyArithmeticException(100, 0);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("LOG: " + e.getMessage());
        }
        catch (RuntimeException e) {
            System.out.println("LOG: " + e.getMessage());
        }
        catch (MyArithmeticException e) {
            System.out.println("LOG: " + e.getMessage());
        }
//        catch (MyArithmeticException | IndexOutOfBoundsException e) {
//            System.out.println("LOG: " + e.getMessage());
//        }
    }
    
    public static void simpleRuntimeException() {
        throw new RuntimeException("my message");
    }
    
    public static void someMethodThrowArrayOutOfBoundException() {
        int[] arr = new int[10];
        System.out.println(arr[10]);
    }
    
    public static void toDivideThrowMyArithmeticException(int a, int b)
          throws MyArithmeticException {
        try {
            System.out.println(a / b);
        }
        catch (ArithmeticException e) {
            throw new MyArithmeticException("Деление на 0!");
        }
    }
}
