package professional.week1.exceptions.task2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileOutput {
    private static final String PKG_DIRECTORY = "/Users/andreigavrilov/Work/Projects/СберКурсыШкола/Java15/src/prof/week1" +
                                                "/exceptions/file";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    
    public static void main(String[] args) throws IOException {
//        Writer writer = null;
//        try {
//            writer = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME);
//            writer.write("Hello!");
//        }
//        catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//        finally {
//            writer.close();
//        }
        
        try (Writer writer1 = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME)) {
            writer1.write("Hello!");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
