package professional.consult.hw3.task3;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) {
        
        Class<APrinter> cls = APrinter.class;
        
        try {
            cls.getMethod("print", int.class).invoke(cls.getConstructor().newInstance(), 5);
        }
        catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            System.out.println(e.getMessage());
        }
        
        /*
        try {

            // создаем объект класса
            APrinter aPrinter = new APrinter();
            Class<? extends APrinter> myClass = aPrinter.getClass();
            // получаем массив методов класса
            Method[] methods = myClass.getMethods();
            // смотрим все методы класса, ищем нужный нам метод
            for (Method method : methods) {
                System.out.println(method);
            }
            // нашли нужный метод print(int), модификатор public, тип void (не возвращает значения),
            // параметр метода имеет тип int
            Method print = myClass.getMethod("print", int.class); // создаем метод print(int)
            int arg = 10; // задаем значение аргумента метода
            System.out.print("\r\nРезультат работы метода '" + print.getName() + "' с аргументом '" + arg + "' : ");
            print.invoke(aPrinter, 10); // вызываем метод на объекте класса

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            System.out.println(e.getMessage());
        }
    }

        
        
         */
        
    }
    
}
