package professional.consult.hw4;

import java.util.List;
import java.util.function.Predicate;

/*
На вход подается список строк. Необходимо вывести количество непустых строк в списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */
public class Task3 {
    public static void main(String[] args) {
        System.out.println(List.of("abc", "", "", "def", "qqq")
                                 .stream().filter(s -> s.length() > 0).count());
    }
    
    private static long calc(List<String> list) {
        return list.stream()
              .filter(Predicate.not(String::isBlank)).count();
    }
    
    //task4
    /*
    List.of(-25.6, -10.31, -10.3, 0.89, 0.895, 0.1)
              .stream()
              .sorted(Collections.reverseOrder())
              .forEach(System.out::println);
     */
    //task5
    /*
     System.out.println(List.of("abc", "def", "qqq")
                                 .stream()
                                 .map(String::toUpperCase)
                                 .collect(Collectors.joining(", ")));
     */
    //task6
    /*
    private static Set<Integer> convertSet(Set<Set<Integer>> sets) {
        return sets.stream()
              .flatMap(Collection::stream)
              .collect(Collectors.toSet());
    }
     */
}
