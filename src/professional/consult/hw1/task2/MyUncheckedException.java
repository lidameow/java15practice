package professional.consult.hw1.task2;

public class MyUncheckedException
      extends RuntimeException {
    
    public MyUncheckedException() {
    }
    
    public MyUncheckedException(String message) {
        super(message);
    }
    
}
