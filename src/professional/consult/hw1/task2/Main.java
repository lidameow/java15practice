package professional.consult.hw1.task2;

/*
* Задача 2
* Создать собственное исключение MyUncheckedException, являющееся
* непроверяемым.
*/
public class Main {
    public static void main(String[] args) {
        methodZero(0);
        methodZero(1);
    }
    
    public static void methodZero(int i) {
        if (i == 0) {
            System.out.println("0");
        } else {
            throw new MyUncheckedException("this is not zero");
        }
    }
    
}
