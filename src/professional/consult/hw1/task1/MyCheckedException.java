package professional.consult.hw1.task1;

public class MyCheckedException
//    extends  RuntimeError/RuntimeException - unchecked
      extends Exception {
    public MyCheckedException(String message) {
        super(message);
    }
}
