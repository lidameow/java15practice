package professional.consult.hw1.task3;

import java.io.*;
import java.util.Scanner;

/*
Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл ./output.txt текст из input,
 где каждый латинский строчный символ заменен на
соответствующий заглавный. Обязательно использование try с ресурсами.

 */
public class FileRearedAndWriter {
    private static final String PKG_DIRECTORY = "src/professional/consult/hw1/task3";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private static final String INPUT_FILE_NAME = "input.txt";
    
    public static void main(String[] args) throws IOException {
        readAndWrite();
    }
    
    public static void readAndWrite() throws IOException {
        Scanner scanner = new Scanner(new File(PKG_DIRECTORY + "/" + INPUT_FILE_NAME));
        Writer writer = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME);
        
        try (scanner; writer) {
            while (scanner.hasNext()) {
                writer.write(scanner.nextLine().toUpperCase() + "\n");
            }
        }
    }
}
