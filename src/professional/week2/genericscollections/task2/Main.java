package professional.week2.genericscollections.task2;

import professional.week2.genericscollections.task2.Pair;

/*
Написать для Pair геттеры/сеттеры
 */
public class Main {
    public static void main(String[] args) {
        Pair<String, Integer> pair = new Pair<>();
        pair.setFirst("First Value");
        pair.setSecond(123);
        System.out.println(pair);
    }
}
