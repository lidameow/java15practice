package professional.week2.genericscollections.task2;

public class Pair<T extends String, U extends Number> {
    private T first;
    private U second;
    
    public T getFirst() {
        return first;
    }
    
    public void setFirst(T value) {
        this.first = value;
    }
    
    public U getSecond() {
        return second;
    }
    
    public void setSecond(U value) {
        this.second = value;
    }
    
    public void print() {
        System.out.println("First: " + first + ", Second: " + second);
    }
    
    @Override
    public String toString() {
        return "Pair{" +
               "first='" + first + '\'' +
               ", second=" + second +
               '}';
    }
}
