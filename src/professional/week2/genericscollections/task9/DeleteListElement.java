package professional.week2.genericscollections.task9;

import java.util.ArrayList;
import java.util.Iterator;

/*
Удалить элемент из списка, если он нечетный.
 */
public class DeleteListElement {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        
        //ConcurrentModificationException
        for (Integer elem : list) {
            if (elem % 2 != 0) {
                list.remove(elem);
            }
        }
        
        //1 способ
        list.removeIf(integer -> integer % 2 != 0);
        
        //2 способ - создать копию списка и работать с ним
        //3 способ - iterator
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            int element = iterator.next();
            if (element % 2 != 0) {
                iterator.remove();
            }
        }
        //4 способ
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 != 0) {
                list.remove(list.get(i));
            }
        }
    }
    //TODO: FAIL-FIRST/ FAIL-FAST итераторы
    //https://itsobes.ru/JavaSobes/chto-takoe-fail-fast-i-fail-safe-iteratory/
    //https://www.baeldung.com/java-fail-safe-vs-fail-fast-iterator
}
