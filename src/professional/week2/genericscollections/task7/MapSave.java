package professional.week2.genericscollections.task7;

import java.util.HashMap;
import java.util.Map;

/*
Простая задача: сохранить в мапе три элемента (1, “first”).
Вывести элемент значение по ключу 2
 */
public class MapSave {
    public static void main(String[] args) {
        Map<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "first");
        hashMap.put(2, "second");
        hashMap.put(3, "third");
//        hashMap.put(2, "smth");
        System.out.println("MAP Elements: " + hashMap);
        System.out.println("Значение по ключу 2: " + hashMap.get(2));
    }
}
