package professional.week2.genericscollections.task5;

import com.sun.source.tree.Tree;

import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;

/*
Создать метод, переводящий из HashSet в TreeSet. Вывести оба варианта.
TODO: Прочитать обязательно про коллекции и деревья
Про деревья:
 https://habr.com/ru/post/330644/
 
Структуры данных в картинках:
  https://habr.com/ru/post/128017/
  
Книга:
  Структуры данных и алгоритмы - Роберт Лафоре
 */
public class ConvertHashSet {
    private ConvertHashSet() {
    }
    
    public static <T> TreeSet<T> convertHashSet(HashSet<T> from) {
        //TreeSet<T> result = new TreeSet<>(from);
        //result.addAll(from);
//        for (T elem : from) {
//            result.add(elem);
//        }
        //return result;
        return new TreeSet<>(from);
    }
}
