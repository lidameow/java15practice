package professional.week2.genericscollections.task1;

import java.io.FileReader;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.function.Function;

/*
Создать класс Pair, который умеет хранить два значения:
1) Любого типа (T, V)
2) Одинакового типа
3) строка и число только
 */
public class Main {
    public static void main(String[] args) {
//        Pair<String, Integer> pair1 = new Pair<>();
//        pair1.first = "Test String";
//        pair1.second = 1;
//        pair1.print();
//
//        Pair<Double, String> pair2 = new Pair<>();
//        pair2.first = 1.0d;
//        pair2.second = "Test String 2";
//        pair2.print();
//        Pair<String> pair = new Pair<>();
//        pair.first = "First";
//        pair.second = "Second";
//        pair.print();
        Pair<String, Double> pair = new Pair<>();
        Pair<String, Integer> pair1 = new Pair<>();
        Pair<String, Float> pair2 = new Pair<>();
        Pair<String, Number> pair3 = new Pair<>();
    }
}
