package professional.week2.genericscollections.task3;

import java.util.*;

/*
 На вход подаются два сета, вывести уникальные элементы,
 которые встречаются и в первом и во втором.
 */
public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);
        System.out.println(set1);
        //union
        //union all
        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(5);
        set2.add(3);
        set2.add(2);
        set2.add(2);
        set1.retainAll(set2);
        
        for (Integer value : set1) {
            System.out.println(value);
        }
        
        //set1.forEach(elem -> System.out.println(elem));
        //set1.forEach(System.out::println);
        //вернет true если нет общих элементов в двух коллекциях
        //Collections.disjoint()
    }
}
