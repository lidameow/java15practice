package professional.week2.genericscollections.task4;

import java.util.*;

/*
Реализовать метод, который считает количество элементов в переданном List
 */
public class Main {
    static class A {}
    public static void main(String[] args) {
        Person p = new Person("FirstName", "LastName", 30);
        System.out.println(p);
        System.out.println(A.class.equals((new A()).getClass()));
        System.out.println((new A()).getClass());
//        TreeMap<Person, Integer> personTreeMap = new TreeMap<>(Comparator.comparingInt(Person::getAge));
//        personTreeMap.put(new Person("Person1Name", "Person1LastName", 17), 0);
//        personTreeMap.put(new Person("Person2Name", "Person2LastName", 60), 0);
//        personTreeMap.put(new Person("Person3Name", "Person3LastName", 18), 0);
//        personTreeMap.put(new Person("Person4Name", "Person4LastName", 20), 2);
//
//        Person myPerson = personTreeMap.navigableKeySet().stream().filter(p -> p.getAge() > 18).findFirst().get();
//        Map<Person, Integer> map1 = personTreeMap.headMap(myPerson, false);
//        Map<Person, Integer> map2 = personTreeMap.tailMap(myPerson, true);
//        System.out.println("Map1: " + map1);
//        System.out.println("Map2: " + map2);
//        List<Boolean> list1 = new ArrayList<>();
//        list1.add(true);
//        list1.add(true);
//        list1.add(false);
//        list1.trimToSize();
//        System.out.println(ListUtils.countIf(list1, true));
//        System.out.println(ListUtils.countIf(list1, false));

//        List<String> list2 = new ArrayList<>();
//        list2.add("abc");
//        list2.add("qqq");
//        list2.add("qwerty");
//        list2.add("qwerty");
//        System.out.println(ListUtils.countIf(list2, "qwerty"));
//        System.out.println(ListUtils.countIf(list2, new String("qwerty")));
        //TODO: Говорим про STRING POOL
        //https://topjava.ru/blog/rukovodstvo-po-string-pool-v-java
        String str1 = "TopJava";
        String str2 = "TopJava";
        String str3 = (new String("TopJava")).intern();
        String str4 = (new String("TopJava")).intern();
        
        System.out.println("Строка 1 равна строке 2? " + (str1 == str2));
        System.out.println("Строка 2 равна строке 3? " + (str2 == str3));
        System.out.println("Строка 3 равна строке 4? " + (str3 == str4));
        
    }
    
}
