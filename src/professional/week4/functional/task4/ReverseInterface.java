package professional.week4.functional.task4;

@FunctionalInterface
public interface ReverseInterface {
    
    public abstract String getReversedString(String initialString);
}
