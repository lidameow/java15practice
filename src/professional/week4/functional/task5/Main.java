package professional.week4.functional.task5;

import professional.week4.functional.MyFunctionalInterface;

/*
Создать параметризованный функциональный интерфейс (Generics)
На выходе получить:
1) Строку наоборот
2) Факториал числа
 */
public class Main {
    public static void main(String[] args) {
        //reverse
        MySuperInterface<String> reversedString = (str) -> new StringBuilder(str).reverse().toString();
        System.out.println(reversedString.func("Lambda"));
        
        //factorial
        MySuperInterface<Integer> factorial = (Integer n) -> {
            int result = 1;
            for (int i = 1; i <= n; i++) {
                result *= i;
            }
            return result;
        };
        System.out.println(factorial.func(5));
        
    }
}


//public class Main {
//    public static void main(String[] args) {
//        Function<Integer, String> convertToString = (x) -> {
//            if (x < 0) {
//                return "отрицательное";
//            } else if (x == 0) {
//                return "ноль";
//            } else {
//                return "положительное";
//            }
//        };
//
//        int[] numbers = {-1, 0, 1};
//
//        for (int number : numbers) {
//            System.out.println("Число " + number + " является " + convertToString.apply(number));
//        }
//    }
//}