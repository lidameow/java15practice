package professional.week4.functional;

public class FunctionalInterfaceExample {
    public static void main(String[] args) {
        //анонимный класс
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("я только создал функциональный интерфейс");
            }
        }).start();
        //(parameter list) -> lambda body
        new Thread(() -> System.out.println("я только создал функциональный интерфейс")).start();
    }
}
