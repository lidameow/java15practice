package professional.week4.functional.task2;

/*
С помощью функционального интерфейса выполнить подсчет квадрата числа
 */
public class Main {
    
    public static void main(String[] args) {
        Square s = new Square() {
            @Override
            public int calculateSquare(int x) {
                return x * x;
            }
        };
        
        System.out.println(s.calculateSquare(3));
        
        Square sl = (x) -> x * x;
        System.out.println(sl.calculateSquare(3));
        
        
    }
}
