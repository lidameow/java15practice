package professional.week4.functional.task1;

import professional.week3reflectionannotations.reflection.superclass.A;

import java.util.ArrayList;

public class Timer {
    public long timeNanoSeconds = 0;
    public void measureTime(Runnable runnable) {
        long startTime = System.nanoTime();
        runnable.run();
        timeNanoSeconds = System.nanoTime() - startTime;
    }
}
