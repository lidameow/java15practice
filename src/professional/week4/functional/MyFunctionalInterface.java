package professional.week4.functional;

@FunctionalInterface
public interface MyFunctionalInterface {
    double getValue();
    //SAM - Single Abstract Methods
}
