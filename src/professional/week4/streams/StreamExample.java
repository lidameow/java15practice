package professional.week4.streams;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class StreamExample {
    public static void main(String[] args) {
        List<String> myPlaces = List.of("Nepal, Pokhara", "Nepal, Kathmandu",  "India, Delhi", "USA, New York", "Africa, Nigeria");
        //Потребитель (Consumer) - это функциональный интерфейс, который принимает один параметр на вход и не возвращает никаких выходных данных
        //Поставщик (Supplier)
        //Предикат (Predicate)
        //Функция (Function)
        myPlaces.stream()
              .filter((place) -> place.startsWith("Nepal"))
              //.map((place) -> place.toUpperCase())
              //String::toUpperCase - является ссылкой на метод, который эквивалентен лямбда-выражению (place) -> place.toUpperCase()
              .map(String::toUpperCase)
              .sorted()
//              .forEach((place) -> System.out.println(place))
              .forEach(System.out::println);
        
        
    }
}
